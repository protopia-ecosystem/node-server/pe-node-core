import TokenType from './TokenType';

export default class AssertionTokenType extends TokenType {
  async createToken(issuer, client) {
    this.lifeTime = 60 * 60;
    const issued_at = Date.now();
    const expires_at = issued_at + this.lifeTime;

    this.json = {
      sub: client._id,
      aud: [client.url, issuer.url],
      iss: issuer.url,
      iat: issued_at,
      exp: expires_at,
    };

    // this.client_secret = this.createKey();

    const string = await JSON.stringify(this.json);
    this.payload = await Buffer.from(string);
  }

  async createSignToken(server, client, user = {}, scope = {}) {
    await this.createToken(server, client, user, scope);

    return await this.sign();
  }

  async createCryptToken(server, client, user = {}, scope = {}) {
    await this.createToken(server, client, user, scope);

    return await this.encrypt();
  }
}

// подписывается ключом клиента

//                {
//                     "iss": "t.me",
//                     "sub": telegram_user_id,
//                     "aud": server_id,
//                     "exp": "",
//                     "nonce": ""
//                 }

// iss (эмитент) — обязательное поле. Адрес HTTPS: URI с указанием полного имени хоста эмитента, который в паре с user_id, создает глобально уникальный и никогда непереназначаемый идентификатор. Например, "https://aol.com", "https://google.com", или "https://sakimura.org".
// sub — обязательное поле. Локально уникальный и никогда непереназначаемый идентификатор для пользователя (субъекта). Например, "24400320"
// aud (аудитория) — обязательное поле. Идентификатор клиента (сlient_id) для которого, этот id_token предназначен.
// ехр (окончание) — обязательное поле. Время, после которого не может быть принят этот маркер.
// nonce — обязательное поле. Установленное сервером значение отправленное в запросе.

/**
 * Generate a JWT
 *
 * @param $privateKey The private key to use to sign the token
 * @param $iss The Issuer, usually the URI for the oauth server
 * @param $sub The Subject, usually a user_id or client_id
 * @param $aud The Audience, usually URI
 * @param $exp The Expiration date. If the current time is greater than the exp, the JWT is invalid
 * @param $iat The "Issued at" date.
 * @param $nbf The "not before" time. If the current time is less than the nbf, the JWT is invalid
 * @param $ati The "Assertion ID", or nonce for this JWT
 *
 * @return string
 */
