import createClientKey from './createClientKey';

const jose = require('node-jose');

export default async function (clients) {
  const keystore = await jose.JWK.createKeyStore();

  for (const client of clients) {
    const key = await createClientKey(client);
    await keystore.add(key);
  }

  return keystore;
}
