import {
  existsSync, readdirSync, readFileSync, statSync,
} from 'fs';

const path = require('path');

export default function () {
  if (global.grants) {
    return global.grants;
  }

  let grants = [];
  const dir = `${__dirname}/../../module`;
  const files = readdirSync(dir);
  for (const file of files) {
    const stat = statSync(path.join(dir, file));
    if (stat.isDirectory()) {
      if (existsSync(path.join(dir, file, '/grants.json'))) {
        const module_grants = require(path.join(dir, file, '/grants.json'));
        grants = grants.concat(module_grants);
      }
    }
  }

  global.grants = grants;

  return grants;
}
