import {
  installModules, cloneModule, cloneLib, createClients,
  createUsers, createAssertionFile, createClientConfig, acceptInstall,
} from './functions';
import modules from '../../config/modules.json';
import clients from '../../config/clients.json';
import users from '../../config/users.json';

async function main() {
  // cloneLib("https://gitlab.com/protopiahome/pe-modules/token-types", "token-types");

  await acceptInstall();

  console.log('Create clients...');
  await createClients(clients);
  console.log('Create users...');
  await createUsers(users);
  console.log('Create assertions...');
  await createAssertionFile();
  console.log('Create client config...');
  await createClientConfig();
  await installModules(modules);

  process.exit();
  // process.end();
}

main();
