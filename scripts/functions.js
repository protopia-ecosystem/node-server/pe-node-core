import bcrypt from 'bcrypt';
import createClientKey from '../server/token-types/createClientKey';
import AssertionTokenType from '../server/token-types/AssertionTokenType';
import createClientsKeyStore from '../server/token-types/createClientsKeyStore';
import db from '../server/db';

const path = require('path');
const crypto = require('crypto');
const child_process = require('child_process');
const fs = require('fs');
const readline = require('readline');

const commandLineArgs = require('command-line-args');

const argsDefinitions = [
  { name: 'yes', alias: 'y', type: Boolean },
  { name: 'app_type', type: String },
  { name: 'db_name', type: String },
  { name: 'db_host', type: String },
  { name: 'http_host', type: String },
  { name: 'http_port', type: Number },
  { name: 'ci', type: Boolean },
];
const args = commandLineArgs(argsDefinitions);

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function ask(question, defaultValue) {
  return new Promise((resolve, reject) => {
    rl.question(question, (input) => resolve(input !== '' ? input : defaultValue));
  });
}

export async function acceptInstall() {
  if (!(args.yes)) {
    if (await ask('DB data will be lost. Continue? [y/N] ') !== 'y') {
      process.exit();
    }
  }
}

async function askOrArgs(name, title, defaultValue) {
  if (args[name]) {
    return args[name];
  }
  return await ask(`${title} (${defaultValue}): `, defaultValue);
}

export async function config() {
  const directory_from = `${__dirname}/template/config`;
  const directory = `${__dirname}/../../config`;

  const db_name = await askOrArgs('db_name', 'DB name', 'pe');
  const http_host = await askOrArgs('http_host', 'HTTP host', 'localhost');
  const http_port = await askOrArgs('http_port', 'HTTP port', '9095');
  const application_type = await askOrArgs('app_type', 'Application type', 'server');

  const db_config = {
    host: args.db_host ? args.db_host : '127.0.0.1',
    port: '27017',
    user: '',
    password: '',
    database: db_name,
  };

  const server_config = {
    host: http_host,
    port: http_port,
    application_type,
  };

  const clients = [
    {
      client_id: server_config.application_type,
      client_name: server_config.application_type,
      application_type: server_config.application_type,
      grant_types: ['client_credentials'],
      contacts: '',
      uri: `http://${server_config.host}:${server_config.port}`,
      external_system: '',
      external_token: '',
    },
    {
      client_id: 'telegram_bot',
      client_name: 'telegram_bot',
      application_type: 'telegram_bot',
      grant_types: ['client_credentials', 'implicit'],
      contacts: '',
      uri: '',
      external_system: 'telegram',
      external_token: '',
    },

    {
      client_id: 'web_client',
      client_name: 'web_client',
      application_type: 'web_client',
      grant_types: ['client_credentials', 'implicit'],
      contacts: '',
      uri: '',
      basic_domain_uris: [
        'localhost',
      ],
      external_system: '',
      external_token: '',
    },
  ];

  const users = [{
    name: 'Admin',
    roles: ['admin'],
    email: 'admin@protopia-ecosystem.net',
    crypto_password: bcrypt.hashSync('admin', 10),
  }];

  fs.writeFileSync(path.join(directory, '/db_config.json'), JSON.stringify(db_config, null, 2), 'utf8');
  fs.writeFileSync(path.join(directory, '/server_config.json'), JSON.stringify(server_config, null, 2), 'utf8');
  fs.writeFileSync(path.join(directory, '/clients.json'), JSON.stringify(clients, null, 2), 'utf8');
  fs.writeFileSync(path.join(directory, '/users.json'), JSON.stringify(users, null, 2), 'utf8');
  if (args.ci && process.env.CI_COMMIT_BRANCH) {
    const branch_file = `${directory_from}/modules_ci_${process.env.CI_COMMIT_BRANCH}.json`;
    console.log(`For CI: load modules from file ${branch_file}`);
    if (fs.existsSync(branch_file)) {
      fs.copyFileSync(branch_file, `${directory}/modules.json`);
    }
  }
  if (!fs.existsSync(`${directory}/modules.json`)) {
    fs.copyFileSync(`${directory_from}/modules.json`, `${directory}/modules.json`);
  }
  process.exit();
}

export async function createClients(clients) {
  if ((await db.client.find()).length) {
    await db.client.drop();
  }

  for (const client of clients) {
    // require('crypto').randomBytes(48, function(err, buffer) { var token = buffer.toString('hex'); console.log(token); });
    client.client_secret = crypto.randomBytes(48).toString('hex');
    await db.client.insert(client);

    // await db.client.findAndModify({
    //     query: {_id: new ObjectId(client._id)},
    //     update: {
    //         $set : client
    //     }
    // })
  }
}

export async function createSecretKeys() {
  const clients = await db.client.find();
  clients.forEach((client) => {
    client.client_secret = crypto.randomBytes(48).toString('hex');
    db.client.findAndModify({
      query: { _id: new ObjectId(client._id) },
      update: {
        $set: { client_secret: key_secret },
      },
    });
  });
  console.log('secrets updated');
}

export async function createUsers(users) {
  if ((await db.user.find()).length) {
    await db.user.drop();
  }

  for (const user of users) {
    // user.register_time = new Date();
    await db.user.insert(user);
    // await db.client.findAndModify({
    //     query: {_id: new ObjectId(user._id)},
    //     update: {
    //         $set : client
    //     }
    // })
  }
}

export async function createAssertionFile() {
  const result = [];
  const clients = await db.client.find();
  for (const client of clients) {
    const key = await createClientKey(client);
    const token = new AssertionTokenType('', key);
    await token.createSignToken(client, client);
    const data = {
      client_id: client._id,
      application_type: client.application_type,
      // client_secret: client.client_secret,
      assertion_token: token.token,
    };
    result.push(data);
    console.log(`kid: ${client._id} ; k: ${client.client_secret}`);
  }

  fs.writeFileSync(`${__dirname}/../../config/assertion.json`, JSON.stringify(result, null, 2), 'utf8');
  console.log('assertion.json writed');
  // process.exit()
}

export async function createClientConfig() {
  const server_config = require(`${__dirname}/../../config/server_config.json`);
  const server = (await db.client.find({ client_name: server_config.application_type }))[0];
  const client = (await db.client.find({ client_name: 'web_client' }))[0];
  const assertions = require(`${__dirname}/../../config/assertion.json`);
  let assertion;
  for (const k in assertions) {
    if (assertions[k].client_id == client._id) {
      assertion = assertions[k];
      break;
    }
  }
  const result = {
    app_url: 'http://localhost:3000',
    server_url: `${server.uri}/graphql`,
    link_type: 'http',
    assertion_token: assertion.assertion_token,
    app_layouts: 'layouts.json',
  };
  fs.writeFileSync(`${__dirname}/../../config/client/config.json`, JSON.stringify(result, null, 2), 'utf8');
  console.log('Copy server/config/client/config.json file to your react client src/config directory.');
}

export async function testAssertionFile() {
  // const token = new AssertionTokenType();
// const key = TokenType.toKey();
// token.setKey(key);
// console.log(TokenType.asKey(key));
// token.sign();

  const clients = await db.client.find();
  const keystore = await createClientsKeyStore(clients);
  console.log(keystore.toJSON(true));

  const file = 'assertion.json';
  const dir = __dirname;
  const assertion_tokens = JSON.parse(fs.readFileSync(path.join(dir, file), 'utf8'));

  for (const assertion_token of assertion_tokens) {
    const assertion = assertion_token.assertion_token;
    // const client_secret = assertion_token.client_secret;
    const { client_id } = assertion_token;

    const token = new AssertionTokenType(assertion, keystore);
    const t = await token.verify();
    const key_json = t.key.toJSON(true);
    console.log(`kid: ${key_json.kid} ; client_id: ${client_id}`);
  }
  console.log('keystore approve assertion.json');

  // process.exit();
}

export async function updateToken() {
  const clients = await db.client.find();
  for (const client of clients) {
    const key = await createClientKey(client);
    const key_secret = key.toJSON(true).k;
    await db.client.findAndModify({
      query: { _id: new ObjectId(client._id) },
      update: {
        $set: { client_secret: key_secret },
      },
    });
    console.log(`kid: ${client._id} ; k: ${key_secret}`);
  }
  console.log('secrets updated');
}

export function cloneLib(url, directory) {
  directory = `${__dirname}/../${directory}`;
  if (!fs.existsSync(directory)) {
    child_process.execSync(`git clone ${url} "${directory}"`);
  }
}

export function updateLib(directory) {
  directory = `${__dirname}/../${directory}`;
  if (fs.existsSync(directory)) {
    child_process.execSync(`cd ${directory} && git pull`);
  }
}

export function cloneModule(url, directory, branch) {
  directory = `${__dirname}/../../module/${directory}`;
  if (!branch) branch = 'master';
  if (!fs.existsSync(directory)) {
    child_process.execSync(`git clone ${url} -b ${branch} "${directory}"`);
    console.log(`${directory} (${branch}) install`);
  }
}

export function updateModule(url, directory, branch) {
  const full_path = `${__dirname}/../../module/${directory}`;
  if (!branch) branch = 'master';
  if (fs.existsSync(full_path)) {
    child_process.execSync(`cd ${full_path} && git fetch && git checkout ${branch} && git pull`);
    console.log(`${directory} (${branch}) update`);
  } else {
    cloneModule(url, directory, branch);
  }
}

export function updateModuleNpm(directory) {
  const full_path = `${__dirname}/../../module/${directory}`;
  const package_json_path = `${full_path}/package.json`;
  if (fs.existsSync(full_path)) {
    if (fs.existsSync(package_json_path)) {
      child_process.execSync(`cd ${full_path} && npm install`);
      console.log(`${directory} npm update`);
    }
  }
}

export async function installModules(modules) {
  console.log('install modules...');
  if (modules) {
    modules.forEach((e, i) => {
      if (e.url && e.name) {
        cloneModule(e.url, e.name, e.branch);
        updateModuleNpm(e.name);
      }
    });
  }
}

export async function updateModules(modules) {
  console.log('update modules...');
  if (modules) {
    modules.forEach((e, i) => {
      if (e.url && e.name) {
        updateModule(e.url, e.name, e.branch);
        updateModuleNpm(e.name);
      }
    });
  }
}
